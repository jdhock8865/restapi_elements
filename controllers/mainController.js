function home(req, res) {
    const welcomeMessage = "Welcome to the Periodic Table of Elements API";
    console.log('Sending the Welcome Message');
    res.send(welcomeMessage);
}

module.exports = {
    home: home
}