const models = require('../models');

// verified working on 01/12/2024
function getElementStates(req, res){
    models.ElementState.findAll().then(result => {
        res.status(200).json(result);
    }).catch(error => {
        res.status(500).json({
            message: "Something went wrong"
        });
    });
}

module.exports = {
    getElementStates: getElementStates 
}