const models = require('../models');

// verified working on 01/12/2024
function getAllElements(req, res){
    models.Element.findAll().then(result => {
        res.status(200).json(result);
    }).catch(error => {
        res.status(500).json({
            message: "Something went wrong"
        });
    });
}

// verified working on 01/12/2024
function getElementById(req, res){
    const id = req.params.id;

    models.Element.findByPk(id).then(result => {
        console.log("Result Length: " + result);
        if(result){
            res.status(200).json(result);
        } else {
            res.status(404).json({
                message: "Element Not Found!"
            });
        }
    }).catch(error => {
        res.status(500).json({
            message: "Something went wrong"
        });
    });
}

// verified working on 01/12/2024
function getElementByAtomicNumber(req, res){
    const atomicNumber = req.params.atomicNumber;
    console.log(atomicNumber);
    
    models.Element.findAll({
        where: {
            atomic_number: atomicNumber
        }
      }).then(result => {
        console.log(result);
        if(result.length != 0){
            res.status(200).json(result);
        } else {
            res.status(404).json({
                message: "Element Not Found!"
            });
        }
      }).catch(error => {
        res.status(500).json({
            message: "Something went wrong"
        });
      });
}

// verified working on 01/12/2024
function insertElement(req, res) {
    const element = {
        atomic_number: req.body.atomic_number,
        atomic_weight: req.body.atomic_weight,
        element_name: req.body.element_name,
        element_symbol: req.body.element_symbol,
        element_state: req.body.element_state,
        element_group: req.body.element_group,
        element_discovery: req.body.element_discovery,
        element_image: req.body.element_image
    }

    models.Element.create(element).then(result => {
        res.status(201).json({
            message: "Element created successfully",
            element: result
        });
    }).catch(error => {
        res.status(500).json({
            message: "Something went wrong",
            error: error
        });
    });
}

// verified working on 01/12/2024
function updateElement(req, res){
    const id = req.params.id;
    const atomicNumber = req.params.atomicNumber;
    console.log(atomicNumber);
    const updatedElement = {
        atomic_number: req.body.atomic_number,
        atomic_weight: req.body.atomic_weight,
        element_name: req.body.element_name,
        element_symbol: req.body.element_symbol,
        element_state: req.body.element_state,
        element_group: req.body.element_group,
        element_discovery: req.body.element_discovery,
        element_image: req.body.element_image
    }
    models.Element.update(updatedElement, {where: {
        atomic_number: atomicNumber
    }
    }).then(result => {
        res.status(200).json({
            message: "Element Successfully Updated",
            element: updatedElement
        })
    }).catch(error => {
        res.status(500).json({
            message: "Something went wrong",
            error: error
        })
    })
}

// verified working on 01/12/2024
function deleteElement(req, res){
    const atomicNumber = req.params.atomicNumber;
    console.log(atomicNumber);
    models.Element.destroy({where: {
        atomic_number: atomicNumber
    }}).then(result => {
        res.status(200).json({
            message: "Element Successfully Deleted",
        })
    }).catch(error => {
        res.status(500).json({
            message: "Something went wrong",
            error: error
        })
    })
}

module.exports = {
    insertElement: insertElement,
    getAllElements: getAllElements,
    getElementById: getElementById,
    getElementByAtomicNumber: getElementByAtomicNumber,
    updateElement: updateElement,
    deleteElement: deleteElement
}
