const express = require('express');
const mainController = require('../controllers/mainController');

const mainRouter = express.Router();

mainRouter.get("/", mainController.home);
mainRouter.get("/api", mainController.home);
mainRouter.get("/api/elements", mainController.home);

module.exports = mainRouter;