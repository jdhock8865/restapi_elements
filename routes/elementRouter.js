const express = require('express');
const elementController = require('../controllers/elementController');

const elementRouter = express.Router();

elementRouter.get("/getElementList", elementController.getAllElements);
elementRouter.get("/getElement/:id", elementController.getElementById);
elementRouter.get("/findElement/:atomicNumber", elementController.getElementByAtomicNumber);
elementRouter.patch("/updateElement/:atomicNumber", elementController.updateElement);
elementRouter.delete("/deleteElement/:atomicNumber", elementController.deleteElement);
elementRouter.post("/insertElement", elementController.insertElement);
module.exports = elementRouter;