const express = require('express');
const elementStateController = require('../controllers/elementStateController');

const elementStateRouter = express.Router();


elementStateRouter.get("/getElementStates", elementStateController.getElementStates);
module.exports = elementStateRouter;