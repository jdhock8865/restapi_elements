const http = require('http');
const app = require('./app');
const port = 9005;

const server = http.createServer(app);

app.listen(port, () => {
    console.log(`The server is listening on port: ${port}`);
});