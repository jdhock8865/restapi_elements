'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Elements', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      atomic_number: {
        type: Sequelize.INTEGER
      },
      atomic_weight: {
        type: Sequelize.DataTypes.DECIMAL(6,3)
      },
      element_name: {
        type: Sequelize.TEXT
      },
      element_symbol: {
        type: Sequelize.TEXT
      },
      element_state: {
        type: Sequelize.TEXT
      },
      element_group: {
        type: Sequelize.INTEGER
      },
      element_discovery: {
        type: Sequelize.TEXT
      },
      element_image: {
        type: Sequelize.TEXT
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Elements');
  }
};