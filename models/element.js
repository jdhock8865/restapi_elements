'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Element extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Element.init({
    atomic_number: DataTypes.INTEGER,
    atomic_weight: DataTypes.DECIMAL,
    element_name: DataTypes.TEXT,
    element_symbol: DataTypes.TEXT,
    element_state: DataTypes.TEXT,
    element_group: DataTypes.INTEGER,
    element_discovery: DataTypes.TEXT,
    element_image: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'Element',
  });
  return Element;
};