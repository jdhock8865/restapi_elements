'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ElementState extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  ElementState.init({
    element_state: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'ElementState',
  });
  return ElementState;
};

// insert into periodic_table.elementstates (element_state, createdAt, updatedAt) values ("solid", current_date, current_date);
// insert into periodic_table.elementstates (element_state, createdAt, updatedAt) values ("liquid", current_date, current_date);
// insert into periodic_table.elementstates (element_state, createdAt, updatedAt) values ("gas", current_date, current_date);
// insert into periodic_table.elementstates (element_state, createdAt, updatedAt) values ("plasma", current_date, current_date);