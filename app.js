// Entry point of the application
// source: https://www.youtube.com/playlist?list=PLG3j59vX4yLHA-wCw7KDP-i0r10ZrckqG - Coder Awesome
// source: https://www.youtube.com/playlist?list=PL_cUvD4qzbkwp6pxx27pqgohrsP8v1Wj2 - Anson the Developer

// Notes
// An applications logic is written in controllers
// An applications end points are defined in routes

// Initialize sequelize
// npx sequelize-cli init

// Create the Element model
// npx sequelize-cli model:generate --name Element --attributes atomic_number:integer,atomic_weight:decimal,element_name:text,element_symbol:text,element_state:text,element_group:integer,element_discovery:text,element_image:text

// Create the table(s) in MySql
// npx sequelize-cli db:migrate

// To start the api open a terminal and enter: npm start

// Populate the Element Table with csv file
/*
LOAD DATA INFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/Periodic Table of Elements.csv'
INTO TABLE elements
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 ROWS;
*/

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

const mainRouter = require('./routes/mainRouter');
const elementRouter = require('./routes/elementRouter');
const elementStateRouter = require('./routes/elementStateRouter');

// Middleware
app.use(cors());

app.use(bodyParser.json());
app.use("/", mainRouter);
app.use("/api", mainRouter);
app.use("/api/elements", mainRouter);
app.use("/api/elements/", elementRouter);
app.use("/api/elements/", elementStateRouter);

module.exports = app;